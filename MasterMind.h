#ifndef MASTERMIND_H
#define MASTERMIND_H

#include <vector>
#include <cstdlib>

#define DEFAULT_NROWS	50
#define DEFAULT_NCOLS	4
#define DEFAULT_NCOLORS	4

enum GameMode
{
	ONE_PLAYER,
	TWO_PLAYER
};

typedef int Piece;

struct GameRow
{
	std::vector<Piece> pieces;
	int numberGoodColor;
	int numberGoodPlace;
};
struct GameState
{
	std::vector<GameRow> rows;
	GameRow solution;
	bool gameEnd;
	bool gameWon;
};

class MasterMind
{
	public:
		MasterMind();
		virtual ~MasterMind();

		GameState getGameState() const;
		int getNumberRows() const;
		int getNumberCols() const;
		int getNumberColors() const;

		void generateSolution();

		void setPieceColor(Piece piece, int color);
		void validateRow();

	protected:
	private:
		GameRow newRow();

		GameMode m_gameMode;
		int m_numberRows;
		int m_numberCols;
		int m_numberColors;

		GameState m_gameState;
};

#endif // MASTERMIND_H
