#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Console.h"
#include "MasterMind.h"

void displayGame(MasterMind masterMind);

Console console;

void displayGame(MasterMind masterMind)
{
	int nrows = masterMind.getNumberRows();
	int ncols = masterMind.getNumberCols();
	int ncolors = masterMind.getNumberColors();
	GameState gameState = masterMind.getGameState();

	bool coloredBackgroundRow = true;
	bool coloredBackgroundSolution = true;

	for(int i = 0; i < gameState.rows.size(); ++i)
	{
		std::cout << i+1 << '\t';
		for(int j = 0; j < gameState.rows[i].pieces.size(); ++j)
		{
			if(coloredBackgroundRow)
			{
				console.setBackgroundColor(static_cast<ConsoleColor>(gameState.rows[i].pieces[j]+1));
				std::cout << ' ';
			}
			else
			{
				console.setForegroundColor(static_cast<ConsoleColor>(gameState.rows[i].pieces[j]+1));
				std::cout << '#';
			}
		}
		console.resetBackgroundColor();
		std::cout << '\t';
		console.setForegroundColor(GREEN);
		std::cout << gameState.rows[i].numberGoodPlace;
		console.resetColor();
		std::cout << " - ";
		console.setForegroundColor(YELLOW);
		std::cout << gameState.rows[i].numberGoodColor;
		if(i == gameState.rows.size()-1 && gameState.gameWon)
		{
			console.setForegroundColor(GREEN);
			std::cout << " <--- WON";
		}

		console.resetColor();
		std::cout << '\n';
	}
	console.resetColor();
	for(int i = gameState.rows.size(); i < nrows; ++i)
	{
		std::cout << i+1 << '\t';
		for(int j = 0; j < ncols; ++j)
		{
			std::cout << '.';
		}
		std::cout << '\n';
	}
	std::cout << "S\t";
	for(int i = 0; i < gameState.solution.pieces.size(); ++i)
	{
		if(gameState.gameEnd)
		{
			if(coloredBackgroundSolution)
			{
				console.setBackgroundColor(static_cast<ConsoleColor>(gameState.solution.pieces[i]+1));
				std::cout << ' ';
			}
			else
			{
				console.setForegroundColor(static_cast<ConsoleColor>(gameState.solution.pieces[i]+1));
				std::cout << '#';
			}
		}
		else
		{
				std::cout << '.';
		}
	}
	console.resetColor();
	std::cout << '\n';
}


void displayColor(int maxColors)
{
	console.setForegroundColor(WHITE);
	for(int i = 0; i < maxColors && i < 15; ++i)
	{
		if(i < 10)
			std::cout << ' ';
		std::cout << i << ' ';
	}
	std::cout << '\n';
	for(int i = 0; i < maxColors && i < 15; ++i)
	{
		console.setBackgroundColor(static_cast<ConsoleColor>(i+1));
		std::cout << "   ";
	}
	console.resetColor();
}

int main()
{
	srand(time(NULL));
    MasterMind masterMind;

	masterMind.generateSolution();

	/// A random game
	int maxColors = masterMind.getNumberColors();
	int numberOfPieces = masterMind.getNumberCols();
	/*while(!masterMind.getGameState().gameEnd)
	{
		for(int i = 0; i < numberOfPieces; ++i)
		{
			system("cls");
			int choice = rand()%maxColors;
			displayGame(masterMind);
			std::cout << "\n\n\n";
			displayColor(maxColors);
			std::cout << "\n\nChoose a color : " << maxColors;
			masterMind.setPieceColor(i, choice);
		}
		masterMind.validateRow();
	}*/

	/// An one player game
	while(!masterMind.getGameState().gameEnd)
	{
		for(int i = 0; i < numberOfPieces; ++i)
		{
			system("cls");
			int choice = -1;
			displayGame(masterMind);
			std::cout << "\n\n\n";
			displayColor(maxColors);
			std::cout << "\n\nChoose a color : ";
			std::cin >> choice;
			masterMind.setPieceColor(i, choice);
		}
		masterMind.validateRow();
	}

	system("cls");
	displayGame(masterMind);

    return 0;
}
