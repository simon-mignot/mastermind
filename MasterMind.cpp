#include "MasterMind.h"

MasterMind::MasterMind()
{
	m_gameMode = ONE_PLAYER;
	m_numberRows = DEFAULT_NROWS;
	m_numberCols = DEFAULT_NCOLS;
	m_numberColors = DEFAULT_NCOLORS;
	m_gameState.gameEnd = false;
	m_gameState.gameWon = false;

	m_gameState.rows.push_back(this->newRow());
}

MasterMind::~MasterMind()
{
	//dtor
}

GameState MasterMind::getGameState() const
{
	return m_gameState;
}

int MasterMind::getNumberRows() const
{
	return m_numberRows;
}
int MasterMind::getNumberCols() const
{
	return m_numberCols;
}
int MasterMind::getNumberColors() const
{
	return m_numberColors;
}

void MasterMind::generateSolution()
{
	m_gameState.solution.numberGoodColor = -1;
	m_gameState.solution.numberGoodPlace = -1;
	m_gameState.solution.pieces.clear();
	for(int i = 0; i < m_numberCols; ++i)
	{
		m_gameState.solution.pieces.push_back(rand()%m_numberColors);
	}
}

void MasterMind::setPieceColor(Piece piece, int color)
{
	if(color >= 0 && color < m_numberColors)
	{
		m_gameState.rows.back().pieces[piece] = color;
	}
}
void MasterMind::validateRow()
{
	GameRow currentRow = m_gameState.rows.back();
	std::vector<Piece> solution = m_gameState.solution.pieces;
	for(int i = 0; i < currentRow.pieces.size(); ++i)
	{
		if(currentRow.pieces[i] == m_gameState.solution.pieces[i])
		{
			++m_gameState.rows.back().numberGoodPlace;
			solution[i] = -1;
			currentRow.pieces[i] = -1;
		}
	}
	for(int i = 0; i < currentRow.pieces.size(); ++i)
	{
		if(currentRow.pieces[i] != -1)
		{
			for(int j = 0; j < solution.size(); ++j)
			{
				if(currentRow.pieces[i] == solution[j])
				{
					solution.erase(solution.begin()+j);
					++m_gameState.rows.back().numberGoodColor;
				}
			}
		}
	}
	if(m_gameState.rows.back().numberGoodPlace == m_numberCols) // All pieces is at good place, win
	{
		m_gameState.gameEnd = true;
		m_gameState.gameWon = true;
	}
	else if(m_gameState.rows.size() >= m_numberRows)	// All rows are full, lose
	{
		m_gameState.gameEnd = true;
		m_gameState.gameWon = false;
	}
	else	// Next row
	{
		m_gameState.rows.push_back(this->newRow());
	}
}


GameRow MasterMind::newRow()
{
	std::vector<Piece> pieces(m_numberCols, -1);
	GameRow ret;
	ret.pieces = pieces;
	ret.numberGoodColor = 0;
	ret.numberGoodPlace = 0;
	return ret;
}



