#ifndef CONSOLE_H
#define CONSOLE_H

#include <windows.h>
#include <iostream>
#include <string>

enum ConsoleColor
{
	BLACK = 0,
	DARK_BLUE,
	GREEN,
	BLUE_GREY,
	BROWN,
	PURPLE,
	KHAKI,
	LIGHT_GREY,
	GREY,
	LIGHT_BLUE,
	LIGHT_GREEN,
	CYAN,
	RED,
	PINK,
	YELLOW,
	WHITE
};

class Console
{
	public:
		Console();
		virtual ~Console();

		void setForegroundColor(ConsoleColor foreground);
		void setBackgroundColor(ConsoleColor background);
		void setColor(ConsoleColor foreground, ConsoleColor background);

		void resetForegroundColor();
		void resetBackgroundColor();
		void resetColor();

		void testColor(std::string stringTest = "Hello Coloured World!");

	protected:
	private:
		HANDLE m_consoleHande;
		ConsoleColor m_foregroundColor;
		ConsoleColor m_backgroundColor;
		ConsoleColor m_defaultForegroundColor;
		ConsoleColor m_defaultBackgroundColor;
};

#endif // CONSOLE_H
