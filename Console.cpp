#include "Console.h"

Console::Console()
{
	m_consoleHande = GetStdHandle(STD_OUTPUT_HANDLE);
	m_defaultForegroundColor = LIGHT_GREY;
	m_defaultBackgroundColor = BLACK;
	m_foregroundColor = m_defaultForegroundColor;
	m_backgroundColor = m_defaultBackgroundColor;
	this->resetColor();
}

Console::~Console()
{
	//dtor
}

void Console::setForegroundColor(ConsoleColor foreground)
{
	this->setColor(foreground, m_backgroundColor);
}
void Console::setBackgroundColor(ConsoleColor background)
{
	this->setColor(m_foregroundColor, background);
}
void Console::setColor(ConsoleColor foreground, ConsoleColor background)
{
	m_foregroundColor = foreground;
	m_backgroundColor = background;
	SetConsoleTextAttribute(m_consoleHande, foreground + background*16);
}

void Console::resetForegroundColor()
{
	this->setForegroundColor(m_defaultForegroundColor);
}
void Console::resetBackgroundColor()
{
	this->setBackgroundColor(m_defaultBackgroundColor);
}
void Console::resetColor()
{
	this->setColor(m_defaultForegroundColor, m_defaultBackgroundColor);
}

void Console::testColor(std::string stringTest)
{
	std::string colorsName[16] = {"BLACK", "DARK_BLUE", "GREEN", "BLUE_GREY", "BROWN", "PURPLE", "KHAKI", "LIGHT_GREY",
								  "GREY", "LIGHT_BLUE", "LIGHT_GREEN", "CYAN", "RED", "PINK", "YELLOW", "WHITE"};
	for(unsigned int i = 0; i < 16; ++i)
		for(unsigned int j = 0; j < 16; ++j)
		{
			this->setColor(static_cast<ConsoleColor>(j), static_cast<ConsoleColor>(i));
			std::cout << stringTest;
			this->resetColor();
			std::cout << '\t' << colorsName[j] << " - " << colorsName[i] << '\n';
		}
}
